Installation/Configuration
==========================
* In "admin/modules" enable "Comments export" module.
* In "admin/structure/views" enable "Comments Export" view.
* In "admin/comments/export" select comment to export.
* From "Operations" drop-down chose "Export comment(s)"
* Press to "Execute" button.
* In file manager navigate to Private site directory. For example:
  "sites/default/files/private"
* In "Private" site directory navigate to "comments_export" directory.
* List exported xml file(s).

Notes
=====
Disqus custom XML import format:
* http://help.disqus.com/customer/portal/articles/472150-custom-xml-import-format