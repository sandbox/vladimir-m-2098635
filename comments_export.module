<?php
/**
 * @file
 * Module comments_export.
 */

/**
 * Implements hook_views_api().
 */
function comments_export_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'comments_export'),
  );
}

/**
 * Implements hook_menu().
 */
function comments_export_menu() {
  $items = array();
  $items['admin/content/comment/comments-export'] = array(
    'title' => 'Comments Export',
    'description' => 'Export comments from the Drupal to XML file (for Disqus).',
    'access arguments' => array('administer content types'),
    'page callback' => 'comments_export_page',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Menu callback.
 */
function comments_export_page() {
  drupal_goto('admin/comments/export');
}

/**
 * Implements hook_action_info().
 */
function comments_export_action_info() {
  return array(
    'comments_export_to_xml_action' => array(
      'type' => 'comment',
      'label' => t('Export comment(s)'),
      'configurable' => FALSE,
      'triggers' => array('any'),
      'hooks' => array('any' => TRUE),
    ),
  );
}

/**
 * Action callback.
 */
function comments_export_to_xml_action($comment) {
  comments_export_cache('nid:' . $comment->nid . ';' . 'cid:' . $comment->cid, $comment);
}

/**
 * Store comments into cache.
 */
function comments_export_cache($key, $value) {
  $variables = & drupal_static(__FUNCTION__);
  if (!isset($variables)) {
    if (!$cache = cache_get($key, 'cache_comments_export')) {
      cache_set($key, $value, 'cache_comments_export');
    }
    else {
      $variables = $cache->data;
    }
  }

  return $variables;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function comments_export_views_bulk_operations_form_alter(&$form, &$form_state, $vbo) {
  global $base_url;

  if ($form_state['step'] == 'views_bulk_operations_confirm_form') {
    $form['comments_export_domain'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#default_value' => variable_get('comments_export_domain', $base_url),
      '#description' => t('Set custom URL address.'),
      '#required' => TRUE,
      '#weight' => -1,
    );
    $form['actions']['submit']['#submit'][] = 'comments_export_views_bulk_operations_form_submit';
  }
}

/**
 * Submit handler.
 */
function comments_export_views_bulk_operations_form_submit($form, &$form_state) {
  $all_cache = db_select('cache_comments_export', 'cce')
    ->fields('cce', array('cid'))
    ->execute()
    ->fetchAll();

  if (!empty($form_state['values']['comments_export_domain'])) {
    $url = $form_state['values']['comments_export_domain'];
    variable_set('comments_export_domain', $url);
  }
  else {
    global $base_url;
    $url = $base_url;
  }

  $random = rand(1000, 100000);
  $xml = array();
  foreach ($all_cache as $cache_key) {
    $cache = cache_get($cache_key->cid, 'cache_comments_export');
    $comment = $cache->data;
    $node = db_select('node', 'n')
      ->fields('n', array('nid', 'title', 'created'))
      ->condition('nid', $comment->nid)
      ->execute()
      ->fetchAssoc();

    if ($comment->mail) {
      $comment_mail = $comment->mail;
    }
    else {
      $comment_mail = "anon" . $random . "@anonymous.com";
    }

    if ($comment->comment_body[$comment->language][0]['value']) {
      $content = _comments_export_content_filter($comment->comment_body[$comment->language][0]['value']);
    }
    else {
      $content = '';
    }

    if (!isset($xml[$comment->nid])) {
      $xml[$comment->nid] = array();
    }
    $xml[$comment->nid]['title'] = _comments_export_cleanse_xml($node['title']);
    $xml[$comment->nid]['link'] = $url . url('node/' . $comment->nid);
    $xml[$comment->nid]['identifier'] = 'node/' . $comment->nid;
    $xml[$comment->nid]['post_date_gmt'] = date("Y-m-d H:i:s", $node['created']);
    $xml[$comment->nid]['status'] = $comment->status;
    $xml[$comment->nid]['comments'][$comment->cid] = array(
      'id' => $comment->cid,
      'author' => $comment->name,
      'author_email' => $comment_mail,
      'author_url' => '',
      'author_IP' => $comment->hostname,
      'date_gmt' => date("Y-m-d H:i:s", $comment->created),
      'content' => '<![CDATA[' . $content . ']]>',
      'approved' => 1,
      'parent' => $comment->pid,
    );
  }
  // Save to XML.
  _comments_export_content_to_xml($xml);
}

/**
 * Save values to XML.
 */
function _comments_export_content_to_xml($xml) {
  if (!empty($xml)) {
    // Pass the data to a function that generates the XML.
    $output = '';

    // Print header.
    $output .= '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
    $output .= '<rss version="2.0"' . "\n";
    $output .= '  xmlns:content="http://purl.org/rss/1.0/modules/content/"' . "\n";
    $output .= '  xmlns:dsq="http://www.disqus.com/"' . "\n";
    $output .= '  xmlns:dc="http://purl.org/dc/elements/1.1/"' . "\n";
    $output .= '  xmlns:wp="http://wordpress.org/export/1.0/"' . "\n";
    $output .= '>' . "\n";
    $output .= '  <channel>' . "\n";

    foreach ($xml as $nid => $thread) {
      if (empty($thread['comments'])) {
        continue;
      }
      $output .= '<item>' . "\n";
      $output .= '<title>' . $thread['title'] . '</title>' . "\n";
      $output .= '<link>' . $thread['link'] . '</link>' . "\n";
      $output .= '<content:encoded></content:encoded>' . "\n";
      $output .= '<dsq:thread_identifier>' . $thread['identifier'] . '</dsq:thread_identifier>' . "\n";
      $output .= '<wp:post_date_gmt>' . $thread['post_date_gmt'] . '</wp:post_date_gmt>' . "\n";
      $output .= '<wp:comment_status>' . ($thread['status'] == 1) ? 'open' : 'closed' . '</wp:comment_status>' . "\n";

      foreach ($thread['comments'] as $comment) {
        $output .= '<wp:comment>' . "\n";
        $output .= '<wp:comment_id>' . $comment['id'] . '</wp:comment_id>' . "\n";
        $output .= '<wp:comment_author>' . $comment['author'] . '</wp:comment_author>' . "\n";
        $output .= '<wp:comment_author_email>' . $comment['author_email'] . '</wp:comment_author_email>' . "\n";
        $output .= '<wp:comment_author_url>' . $comment['author_url'] . '</wp:comment_author_url>' . "\n";
        $output .= '<wp:comment_author_IP>' . $comment['author_IP'] . '</wp:comment_author_IP>' . "\n";
        $output .= '<wp:comment_date_gmt>' . $comment['date_gmt'] . '</wp:comment_date_gmt>' . "\n";
        $output .= '<wp:comment_content>' . $comment['content'] . '</wp:comment_content>' . "\n";
        $output .= '<wp:comment_approved>' . $comment['approved'] . '</wp:comment_approved>' . "\n";
        $output .= '<wp:comment_parent>' . $comment['parent'] . '</wp:comment_parent>' . "\n";
        $output .= '</wp:comment>' . "\n";
      }
      $output .= '</item>' . "\n";
    }
    // Footer.
    $output .= '  </channel>' . "\n";
    $output .= '</rss>' . "\n";

    $filename = 'drupal_comments-' . date("Y-m-d_H-i-s", time()) . '.xml';
    _comments_export_to_xml($output, $filename);
  }
  else {
    drupal_set_message(t('No comments to export.'), 'error');
  }
}

/**
 * Helper function cleanse a string for placement in an XML file.
 */
function _comments_export_cleanse_xml($string) {
  $find = array(
    '>',
    '<',
    '&',
    '"',
    "'",
  );

  $replace = array(
    '&gt;',
    '&lt;',
    '&amp;',
    '&apos;',
    '&quot;',
  );

  return str_replace($find, $replace, $string);
}

/**
 * Scan input and make sure that all HTML tags are properly closed and nested.
 */
function _comments_export_content_filter($text) {
  $text = str_replace("\r", '', $text);
  return filter_dom_serialize(filter_dom_load($text));
}

/**
 * Export content to XML file.
 */
function _comments_export_to_xml($data, $filename) {
  if (!is_dir(drupal_realpath('private://') . '/' . 'comments_export')) {
    mkdir(drupal_realpath('private://') . '/comments_export');
  }

  if (is_dir(drupal_realpath('private://') . '/' . 'comments_export')) {
    $file = drupal_realpath('private://') . '/comments_export/' . $filename;
    $fp = fopen($file, 'w+');

    if (is_writable($file)) {
      fwrite($fp, print_r($data, TRUE));
      fclose($fp);
      drupal_set_message(t('Comment(s) successfully saved in to @file file, click to <a href="@here">download</a> the file.', array(
        '@file' => $filename,
        '@here' => file_create_url('private://comments_export/' . $filename)
      )));
      cache_clear_all('*', 'cache_comments_export', TRUE);
    }
    else {
      drupal_set_message(t('File <em>@filename</em> is not writable.', array('@filename' => $filename)), 'error');
    }
  }
  else {
    drupal_set_message(t('Folder <em>comments_export</em> can not be crated.'), 'error');
  }
}
